<?php
namespace App;

use App\Services\DB\Connection;
use App\Services\Templates\Template;

use App\Controllers\IndexController;

class Core
{

    public $db;
    public $template;

    public function __construct()
    {
        session_start();

        $this->db = new Connection();
        $this->template = new Template();
    }

    /**
     * $_REQUEST['q'] - На реальномпроэкте это плохо, так как мы привязались
     * к определенной настройке web-сервера. Но ведь в тестовом это наоборот плюс!)
     * Показываем, что обладаем минимальными знаниями работы web-сервера!)))
     */
    public function init()
    {
        $this->handleRequest(trim($_REQUEST['q'] ?? ''));
    }

    protected function handleRequest($uri)
    {
        $req = explode('/', $uri);

        /**
         * Разбираемся с контроллером
         */
        $controller = '\App\Controllers\\' . ucfirst(array_shift($req)) . 'Controller';
        if (!class_exists($controller)) {
            $controller = new IndexController($this);
        }
        else {
            $controller = new $controller($this);
        }
        $controller->isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';

        /**
         * Разбираемся с методом
         */
        $method = $req['0'] ?? '';
        if (!method_exists($controller, $method)) {
            $method = 'index';
        }

        /**
         * Выполнение и возврат результата
         */
        echo $controller->{$method}();
    }

    public function ajaxResponse($success = true, $message = '', array $data = array()) {
        $response = array(
            'success' => $success,
            'message' => $message,
            'data' => $data,
        );

        exit(json_encode($response,1));
    }
}