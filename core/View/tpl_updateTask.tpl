<form class="form-update" action="/task/save{$formLink}" method="post">
    <p>Название: <strong>{$task->user_name}</strong></p>
    <p>E-mail: <strong>{$task->email}</strong></p>
    <div class="form-group">
        <textarea required class="form-control" name="content">{$task->content}</textarea>
        <input type="hidden" name="id" value="{$task->id}">
    </div>
    <div class="d-flex justify-content-between align-items-center">
        <a class="form-update__link" href="{$backLink}"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Вернуться на главную</a>
        <input type="submit" name="update" class="btn btn-primary" value="Сохранить">
    </div>
</form>