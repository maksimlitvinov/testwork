{extends '/base/base.tpl'}

{block 'template'}
    <div class="container">
        <div class="row">
            <div class="col-md-hidden col-lg-3 col-xl-3"></div>
            <div class="col-md-12 col-lg-6 col-xl-6">
                <h1>Авторизация</h1>
                {if $error.auth ?}
                    <div class="alert alert-danger" role="alert">
                        {$error.auth}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                {/if}
                <form class="form-auth" action="/auth" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Логин</label>
                        <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите логин">
                        <small id="emailHelp" class="form-text text-danger">{$error.name}</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Пароль</label>
                        <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Введите пароль">
                        <small id="passHelp" class="form-text text-danger">{$error.password}</small>
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <a class="form-auth__link" href="/"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Вернуться на главную</a>
                        <input type="submit" name="auth" class="btn btn-primary" value="Авторизоваться">
                    </div>
                </form>
            </div>
            <div class="col-md-hidden col-lg-3 col-xl-3"></div>
        </div>
    </div>
{/block}