<div id="task_{$task->id}" class="task__item {$task->deleted_at ? 'compleated' : ''}">
    <div class="row">
        <div class="col-12">
            <div class="row d-flex justify-content-between align-items-center">
                <div class="col-9">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-6 col-xl-6">
                            <p>{$task->user_name}</p>
                        </div>
                        <div class="col-12 col-md-6 col-lg-6 col-xl-6">
                            <p>{$task->email}</p>
                        </div>
                        <div class="col-12 col-md-12">
                            <p class="task-content">{$task->content}</p>
                            <p class="form-text text-danger">{$task->edited ? 'Отредактированно Администратором' : ''}</p>
                        </div>
                    </div>
                </div>
                <div class="col-3 text-center">
                    {if !$task->deleted_at ?}
                        <a class="task__item-link task-compleate" href="/task/compleat{$task->url}" title="Отметить выполненной">
                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                        </a>
                        {if $is_auth ?}
                            <a class="task__item-link task-edit" href="/task/edit{$task->url}" title="Отредактировать">
                                <i class="fa fa-pencil-square" aria-hidden="true"></i>
                            </a>
                        {/if}
                    {else}
                        <p>Готово!</p>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>