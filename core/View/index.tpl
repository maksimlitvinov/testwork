{extends '/base/base.tpl'}

{block 'template'}
    <header class="bg-light pb-2 pt-2">
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    {if $is_auth ?}
                        <a class="btn btn-outline-danger" href="/auth/logout">Выйти</a>
                    {else}
                        <a class="btn btn-outline-success" href="/auth">Авторизация</a>
                    {/if}
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Новая задача</h3>
                <form action="/" method="post">
                    <div class="row">
                        <div class="col mb-2">
                            <input required name="user_name" value="{$form.user_name}" type="text" class="form-control" placeholder="Имя">
                            <span class="form-text text-danger">{$errorForm.user_name}</span>
                        </div>
                        <div class="col mb-2">
                            <input name="email" required type="email" value="{$form.email}" class="form-control" placeholder="E-mail">
                            <span class="form-text text-danger">{$errorForm.email}</span>
                        </div>
                        <div class="col-12 mb-2">
                            <textarea required name="content" class="form-control" placeholder="Текст задачи">{$form.content}</textarea>
                            <span class="form-text text-danger">{$errorForm.content}</span>
                        </div>
                        <div class="col-12 text-right">
                            <input name="addTask" type="submit" class="btn btn-success" value="Добавить задачу">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 task__list">
                <h3>Список задач</h3>
                <div class="task__group d-flex">
                    <p class="task__group-link m-2">Сортировать: </p>
                    <a href="{$sortLink.user_name}" class="task__group-link m-2">По имени</a>
                    <a href="{$sortLink.email}" class="task__group-link m-2">По e-mail</a>
                </div>
                {foreach $tasks as $task}
                    {include 'tpl_task.tpl'}
                {/foreach}

                {if $pagination ?}
                    {$pagination}
                {/if}

            </div>
        </div>
    </div>
{/block}