{extends '/base/base.tpl'}

{block 'template'}
    <div class="container task__uform">
        <div class="row">
            <div class="col-12">
                <h1>Редактирование задачи</h1>
                {include 'tpl_updateTask.tpl'}
            </div>
        </div>
    </div>
{/block}