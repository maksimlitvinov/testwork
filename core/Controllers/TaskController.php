<?php
namespace App\Controllers;

use App\Models\Tasks;
use App\Services\Auth\Auth;

class TaskController extends BaseController
{
    /**
     * Добавление задачи
     * @return array
     */
    public function add()
    {

        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect();
        }

        $response = $this->validate($_POST);

        if (!$response['success']) {
            return $response;
        }

        $task = new Tasks;
        $task->fill($response['fields']);
        $task->save();
    }

    public function compleat()
    {
        // Действие при Ajax
        if ($this->isAjax) {
            if (!($_REQUEST['id'] ?? false)) {
                $this->app->ajaxResponse(false, 'Не указан ID записи');
            }
            $task = $this->delete($_REQUEST['id']);

            $this->app->ajaxResponse(true, 'Задача выполненна',[
                'html' => $this->template->render('tpl_task.tpl', ['task' => $task]),
                'id' => $task->id
            ]);
        }

        // Действия без Ajax
        if (!isset($_GET['id'])) {
            // Тут нужно отдавать сообщение
            $this->redirect();
        }
        $this->delete($_GET['id']);
        $this->redirect();
    }

    public function edit()
    {
        if (!Auth::is_auth() || !isset($_GET['id'])) {
            $this->redirect();
        }

        $task = Tasks::find($_GET['id']);
        if (empty($task)) {
            $this->redirect();
        }
        return $this->template->render('updateTask.tpl', [
            'task' => $task,
            'backLink' => $this->taskParams->generateUri([], ['id']),
            'formLink' => $this->taskParams->generateUri()
        ]);
    }

    /**
     * Сохранение приредактировании формы
     */
    public function save()
    {
        if (!Auth::is_auth()) {
            $this->redirect();
        }
        if ($_SERVER['REQUEST_METHOD'] != 'POST' || !isset($_POST['update'])) {
            $this->redirect();
        }

        $data = $this->validate($_POST);
        if (!$data['success']) {
            $this->redirect('/task/edit' . $this->taskParams->generateUri());
        }

        $task = Tasks::find($_POST['id'])->update($data['fields']);
        $this->redirect($this->taskParams->generateUri([], ['id']));
    }

    protected function delete($id)
    {
        $task = Tasks::find($id);
        $task->delete();
        return $task;
    }
}