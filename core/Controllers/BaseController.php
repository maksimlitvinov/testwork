<?php
namespace App\Controllers;

use App\Core;
use App\Services\Request\TaskParams;
use App\Services\Templates\Template;

abstract class BaseController
{
    /**
     * @var Core
     */
    public $app;

    /**
     * @var Template
     */
    public $template;

    /**
     * @var boolean
     */
    public $isAjax;

    /**
     * @var TaskParams
     */
    public $taskParams;

    /**
     * BaseController constructor.
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;
        $this->template = $app->template;
        $this->taskParams = new TaskParams();
    }

    /**
     * Базовый метод. Просто редиректит на главную.
     * Нужен контроллерам для которых нет темплейтов.
     */
    public function index()
    {
        $this->redirect();
    }

    /**
     * Метод редиректа
     * @param string $url
     */
    public function redirect($url = '/') {
        if ($this->isAjax) {
            $this->app->ajaxResponse(false, 'Редирект на другой адрес', array('redirect' => $url));
        }
        else {
            header("Location: {$url}");
            exit();
        }
    }

    /**
     * Пример костыльно-ориентированного программирования
     * Можно было создать фассад для Eloquent Validator и
     * использовать его, но пока решил так.
     *
     * @param array $data
     * @return array
     *
     */
    public function validate(array $data)
    {
        $result = [
            'fields' => [],
            'success' => true,
            'error' => []
        ];
        $form = [];
        foreach ($data as $key => $value) {
            if (!empty($value)) {
                $value = trim($value);
                if ($key == 'email') {
                    $result['fields'][$key] = filter_var($value, FILTER_SANITIZE_EMAIL);
                }
                elseif ($key == 'page') {
                    $result['fields'][$key] = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                }
                elseif ($key == 'password') {
                    $result['fields'][$key] = filter_var($value, FILTER_UNSAFE_RAW);
                }
                else {
                    $result['fields'][$key] = filter_var($value, FILTER_SANITIZE_STRING);
                }
            }
            else {
                $result['success'] = false;
                $result['error'][$key] = 'Данное поле обязательно для заполнения.';
            }

            // Собираем данные на случай неудачно заполненной формы
            $form[$key] = $value;
            if (!$result['success']) {
                $result['form'] = $form;
            }
        }

        return $result;
    }
}