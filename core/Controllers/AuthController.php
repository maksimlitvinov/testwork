<?php
namespace App\Controllers;

use App\Services\Auth\Auth;

use App\Models\Users;

class AuthController extends BaseController
{
    /**
     * Основной метод
     */
    public function index()
    {
        if (Auth::is_auth()) {
            $this->redirect();
        }
        $data = [];
        if ($_POST['auth'] ?? false) {
            $data = $this->login();
        }

        return $this->template->render('authForm', $data ?? []);
    }

    /**
     * Авторизация
     * @return array
     */
    public function login()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $this->redirect('/auth');
        }

        $response = $this->validate($_POST);
        if (!$response['success']) {
            return $response;
        }

        $fields = $response['fields'];

        $user = Users::where('name', $fields['name'])->where('pass', md5($fields['password']));
        if ($user->count() == 0) {
            $response['error']['auth'] = 'Неверное имя пользователя или пароль.';
            return $response;
        }
        $user = $user->first();

        Auth::login($user->name);
        $this->redirect();
    }

    /**
     * Разлогиневаемся
     */
    public function logout()
    {
        Auth::logout();
        $this->redirect();
    }
}