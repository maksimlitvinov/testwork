<?php
namespace App\Controllers;

use App\Models\Users;
use App\Models\Tasks;
use App\Services\Auth\Auth;

class IndexController extends BaseController
{
    public $limit = 3;
    public $page = 1;

    /**
     * Основной метод контроллера.
     * Собирает необходимые данные и
     * рендерит шаблон.
     */
    public function index()
    {
        // Заглушка ответа добавления задачи
        $response = [
            'success' => true,
            'form' => []
        ];

        // Добавляем задачу, если пришли данные из формы
        if ($_POST['addTask'] ?? false) {
            $task = new TaskController($this->app);
            $response = $task->add();
        }

        // Собираем основные данные дляшаблона
        $data = [
            'users' => Users::all(),
            'tasks' => $this->getTasks(),
            'form' => $response['form'],
            'pagination' => $this->getPagination(),
            'sortLink' => [
                'user_name' => $this->generateSortLink('sort', 'user_name'),
                'email' => $this->generateSortLink('sort', 'email')
            ],
            'is_auth' => Auth::is_auth()
        ];

        // Добавляем ошибки
        if (!$response['success']) {
            $data['errorForm'] = $response['error'] ?? [];
        }

        // Рендерим шаблон
        return $this->template->render('index', $data);
    }

    /**
     * Получаем задачи с учетом пагинации
     * @return array
     */
    public function getTasks()
    {
        $params = $this->prepareSort();

        $this->page = $this->getPage();
        $tasks = Tasks::withTrashed()->get();
        if (!empty($params)) {
            if ($params['dir'] == 'asc') {
                return $tasks->sortBy($params['field'])->forPage($this->page, $this->limit);
            }
            else {
                return $tasks->sortByDesc($params['field'])->forPage($this->page, $this->limit);
            }
        }
        return $tasks->sortByDesc('created_at')->forPage($this->page, $this->limit);
    }

    /**
     * @return mixed
     */
    public function getCountTask()
    {
        return Tasks::count();
    }

    /**
     * Получаем пагинацию
     * @return string
     */
    public function getPagination()
    {
        $result = '';
        $taskCount = $this->getCountTask();

        // Разбераемся со страницами
        if ($taskCount > $this->limit) {
            $pageCount = ceil($taskCount / $this->limit);
            for ($i=1; $i <= $pageCount; $i++) {
                $tpl = 'pagination/link';
                if ($i == $this->page) {
                    $tpl = 'pagination/curent';
                }
                $result .= $this->template->render($tpl, [
                    'uri' => $this->taskParams->generateUri(['page' => $i]),
                    'number' => $i
                ]);
            }
        }

        // Оборачиваем в общий чанк
        if (!empty($result)) {
            $result = $this->template->render('pagination/wrap', ['wrap' => $result]);
        }

        return $result;
    }

    /**
     * Получение текущей страницы
     * @return int
     */
    public function getPage()
    {
        $res = $this->validate($_GET);
        $page = $res['fields']['page'] ?? false;
        if ($page == 1) {
            $this->taskParams->rmParam('page');
            $this->redirect($this->taskParams->generateUri());
        }
        return $res['fields']['page'] ?? 1;
    }

    /**
     * Генерация ссылок для сортировки.
     * @param $type
     * @param $field
     * @return string
     */
    public function generateSortLink($type, $field)
    {
        $param = $this->taskParams->getParam($type);
        if (empty($param)) {
            return $this->taskParams->generateUri([$type => $field . ':asc']);
        }
        list($setField, $setDir) = explode(':', $param);
        if ($setField == $field) {
            if (strtolower($setDir) == 'asc') {
                return $this->taskParams->generateUri([$type => $field . ':desc']);
            }
        }
        return $this->taskParams->generateUri([$type => $field . ':asc']);
    }

    /**
     * Получение поля сортировки и направления из URL
     * @return array
     */
    public function prepareSort()
    {
        $param = $this->taskParams->getParam('sort');
        if (!empty($param)) {
            $arr = explode(':', $param);
            return [
                'field' => $arr[0],
                'dir' => strtolower($arr[1])
            ];
        }
        return [];
    }
}