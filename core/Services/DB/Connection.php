<?php
namespace App\Services\DB;

use Illuminate\Database\Capsule\Manager as Capsule;

class Connection
{
    public $capsule;

    public function __construct()
    {
        /**
         * Обычно выносится в отдельный файл. Сейчас не стал морочится,
         * Так как вам, возможно, захочется посмотреть на структуру базы.
         * host = webhost35.ru
         */
        $config = [
            'driver' => 'mysql',
            'host' => 'localhost',
            'port' => '3306',
            'database' => 'admin_testwork',
            'username' => 'admin_testwork',
            'password' => 'Maxim19092002',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => ''
        ];
        $this->capsule = new Capsule();
        $this->capsule->addConnection($config);
        $this->capsule->bootEloquent();
        return $this->capsule;
    }
}