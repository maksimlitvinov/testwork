<?php
namespace App\Services\Auth;

use App\Services\Storage\Coocies;

class Auth
{

    /**
     * @var string
     */
    private static $key = 'user';

    /**
     * Авторизовываемся
     * @param $name
     */
    public static function login($name)
    {
        Coocies::set(self::$key, $name);
    }

    /**
     * Проверка авторизации
     * @return bool
     */
    public static function is_auth()
    {
        return (boolean) Coocies::get(self::$key);
    }

    /**
     * Разлогиниваемся
     */
    public static function logout()
    {
        Coocies::remove(self::$key);
    }
}