<?php
namespace App\Services\Request;

class TaskParams
{

    private $params;

    public function __construct()
    {
        if (isset($_GET['q'])) {
            unset($_GET['q']);
        }
        $this->params = $_GET;
        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getParam($key)
    {
        return $this->params[$key] ?? null;
    }

    public function setParams(array $param)
    {
        $this->params = array_merge($this->params, $param);
    }

    public function rmParam($key)
    {
        if (isset($this->params[$key])) {
            unset($this->params[$key]);
        }
    }

    /**
     * @param array $link
     * @param array $exclude
     * @return string
     */
    public function generateUri(array $link = [], array $exclude = [])
    {
        $parameters = $this->params;
        if (!empty($exclude)) {
            foreach ($this->params as $key => $value) {
                if (in_array($key, $exclude)) {
                    unset($parameters[$key]);
                }
            }
        }
        $param = http_build_query(array_merge($parameters, $link));
        if (!empty($param)) {
            $param = '/?' . $param;
        }
        else {
            $param = '/';
        }
        return $param;
    }
}