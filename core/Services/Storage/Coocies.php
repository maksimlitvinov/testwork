<?php
namespace App\Services\Storage;

class Coocies
{
    /**
     * @param $key
     * @param $value
     * @param int $time
     */
    public static function set($key, $value, $time=86400)
    {
        if (!empty($time)) {
            setcookie($key, $value, time()+$time, '/');
        }
        else {
            setcookie($key, $value);
        }
    }

    /**
     * @param $key
     */
    public static function remove($key)
    {
        if (isset($_COOKIE[$key])) {
            Coocies::set($key, '', -1);
            unset($_COOKIE[$key]);
        }
    }

    /**
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        return $_COOKIE[$key] ?? null;
    }
}