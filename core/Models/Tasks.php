<?php
namespace App\Models;

use App\Models;
use App\Services\Request\TaskParams;

class Tasks extends BaseModel
{
    protected $fillable = ['user_name','email','content',];

    public function getUrlAttribute() {
        $param = new TaskParams();
        return $param->generateUri(['id' => $this->id]);
    }

    public function getEditedAttribute()
    {
        return $this->created_at != $this->updated_at && !$this->deleted_at;
    }
}