var Task = {
    sel: {
        btnCompleate: '.task-compleate',
        linkBack: '.form-update__link'
    },
    classes: {
        compleated: 'compleated'
    },
    init: function () {
        this.load();
    },
    load: function () {
        var self = this;

        $(this.sel.btnCompleate).on('click', function (e) {
            e.preventDefault();
            var result = self.prepareUrl($(this).attr('href'));
            self.send(result.url, result.params, Task.compleateSuccess);
        });
    },
    prepareUrl: function (url) {
        if (url == undefined || url.length == 0) {return false;}
        url = url.split('?');

        // Если некорректная ссылка
        if (url.length < 2) {return false;}
        // Если в ссылке нет id
        if (!(url[1].indexOf('id') + 1)) {return false;}
        // Если в ссылке помимо id есть еще параметры
        if (!!(url[1].indexOf('&') + 1)) {
            var tmp = url[1].split('&');
            url[1] = tmp.filter(function (item) {
                return !!(item.indexOf('id')+1);
            }).pop();
        }
        // Возвращаем объект с данными
        return  {
            'url': url[0],
            'params': url[1]
        };
    },
    compleateSuccess: function(response) {
        var block = $(response.data.html).children();
        $('#task_' + response.data.id).empty().html(block).addClass(Task.classes.compleated);
    },
    send: function (url, param, fn) {
        $.ajax({
            data: param,
            dataType: 'json',
            type: 'POST',
            url: url,
            success: function (data) {
                fn(data);
            }
        });
    }
};

$(document).ready(function () {
    Task.init();
});