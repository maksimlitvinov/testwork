<?php
$dev = $_GET['dev'] ?? false;
if ($dev) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

use App\Core;
require_once 'vendor/autoload.php';

if (!defined('PROJECT_BASE_PATH')) {
    define('PROJECT_BASE_PATH', strtr(realpath(dirname(__FILE__)), '\\', '/') . '/');
}
if (!defined('PROJECT_CORE_PATH')) {
    define('PROJECT_CORE_PATH', PROJECT_BASE_PATH . 'core/');
}
if (!defined('PROJECT_MODEL_PATH')) {
    define('PROJECT_MODEL_PATH', PROJECT_CORE_PATH . 'Models/');
}
if (!defined('PROJECT_TEMPLATES_PATH')) {
    define('PROJECT_TEMPLATES_PATH', PROJECT_CORE_PATH . 'View/');
}
if (!defined('PROJECT_CACHE_PATH')) {
    define('PROJECT_CACHE_PATH', PROJECT_CORE_PATH . 'Cache/');
}
if (!defined('PROJECT_CACHE_TEMPLATE_PATH')) {
    define('PROJECT_CACHE_TEMPLATE_PATH', PROJECT_CACHE_PATH . 'templates/');
}
if (!defined('PROJECT_FENOM_OPTIONS')) {
    define('PROJECT_FENOM_OPTIONS', \Fenom::AUTO_RELOAD | \Fenom::FORCE_VERIFY);
}

$app = new Core();
$app->init();